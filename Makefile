include ${FSLCONFDIR}/default.mk

PROJNAME = cudabasisfield
SOFILES  = libfsl-cudabasisfield_cuda${CUDA_VER}.so
HFILES   = *.h *.cuh
LIBS     = -lfsl-basisfield -lfsl-miscmaths -lfsl-newimage
OBJS     = CBFKernelDefinitions.o CBFSparseDiagonalMatrix.o CBFSplineField.o

all: ${SOFILES}

libfsl-cudabasisfield_cuda${CUDA_VER}.so: ${OBJS}
	${NVCC} ${NVCCFLAGS} -shared -o $@ $^ ${NVCCLDFLAGS}
